<?php
class ProjetDAO{
    public static function lesProjets(){
        $sql = "select * from projet";
        $requete = DBConnex::getInstance()->prepare($sql);
        $requete->execute();
        $listeProjets = $requete->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public static function ajouterProjet(Projet $unProjet){
        $sql = "insert into projet (nomProjet, dureeProjet, dateDebutProjet, dateFinProjet)
        values (:nomProjet, :dureeProjet, :dateDebutProjet, :dateFinProjet);";
        $requete = DBConnex::getInstance()->prepare($sql);

        $nomProjet = $unProjet->getNomProjet();
        $dureeProjet = $unProjet->getDureeProjet();
        $debutProjet = $unProjet->getDateDebutProjet();
        $finProjet = $unProjet->getFinProjet();

        $requete->bindParam("nomProjet", $nomProjet);
        $requete->bindParam("dureeProjet", $dureeProjet);
        $requete->bindParam("dateDebutProjet", $debutProjet);
        $requete->bindParam("dateFinProjet", $finProjet);
        $requete->execute();
        
        return DBConnex::getInstance()->lastInsertId();
    }
}