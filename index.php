<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8" />
    <title>MPM</title>
    <style type="text/css">
        @import url(styles/mpm.css);
    </style>
</head>

<body>
    <div id='content'>
        <div id='projets'>
            <div id='divMenuProjets'>
            </div>
        </div>
        <div id='formulaire'>
            <div id='divFormulaire'>
            </div>
        </div>
        <div id='taches'>
        </div>
    </div>
    <script src='scripts/formulaire.js'></script>
    <script src='scripts/projet.js'></script>
    <script src='scripts/projets.js'></script>
    <script src='scripts/tache.js'></script>
    <script src='scripts/mpm.js'></script>
    <script src='scripts/fetch.js'></script>
</body>

</html>