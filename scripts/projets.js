class Projets {
    constructor() {
        this.projets = [];
    }
}
Projets.prototype.rechercherProjet = function (unId) {
    for (projet of this.projets) {
        if (unId == projet.idProjet) {
            return projet;
        }
    }
}

Projets.prototype.ajouterProjet = function(unProjet) {
    this.projets.push(unProjet);
}