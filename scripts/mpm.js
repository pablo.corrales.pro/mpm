var idProjet = 0;
var formulaireProjets;
var monProjet;
var lesProjets;
function ajouterBoutonNouveauProjet() {
	let leParent = document.getElementById("divMenuProjets");
	formulaireProjets = new Formulaire("menuProjets", "menuProjets", leParent, "menuProjets");
	formulaireProjets.creerFormulaire();
	formulaireProjets.creerBouton('btnNouveauProjet', 'btnNouveauProjet', 'btnProjets', 'Créer nouveau Projet');
	let nouveauProjet = document.getElementById('btnNouveauProjet');
	nouveauProjet.addEventListener("click", function () {
		let parent = document.getElementById('content');
		let divForm = document.getElementById('ajoutProjet');
		if (divForm != null) {
			parent.removeChild(divForm);
		}
		ajouterFormulaireNouveauProjet();
	});
}

function ajouterFormulaireNouveauProjet(){
	let leParent = document.getElementById("divFormulaire");
	let ajoutProjet = new Formulaire("ajoutProjet", "ajoutProjet", leParent, "ajoutProjet");
	ajoutProjet.creerFormulaire();
	ajoutProjet.creerLabel("titreFormProjet", "Créer nouveau projet", "titreFormProjet");
	ajoutProjet.creerLabel("labelNom", "Nom du projet : ", "label");
	ajoutProjet.creerInputTexte("inputNom", "inputNom", "inputNom", "");
	ajoutProjet.retourLigne();
	ajoutProjet.creerLabel("labelDuree", "Durée du projet : ", "label");
	ajoutProjet.creerInputTexte("inputDuree", "inputDuree", "inputDuree", "");
	ajoutProjet.retourLigne();
	ajoutProjet.creerLabel("labelDateDeb", "Date début du projet : ", "label");
	ajoutProjet.creerInputDate("inputDateDeb", "inputDateDeb", "inputDateDeb");
	ajoutProjet.retourLigne();
	ajoutProjet.creerLabel("labelDateFin", "Date fin du projet : ", "label");
	ajoutProjet.creerInputDate("inputDateFin", "inputDateFin", "inputDateFin");
	ajoutProjet.retourLigne();
	ajoutProjet.creerBouton('btnAnnuler', 'btnAnnuler', 'btnFormuProjet', 'Annuler');
	ajoutProjet.creerBouton('btnValider', 'btnValider', 'btnFormuProjet', 'Valider');
	
	let validerProjet = document.getElementById('btnValider');
	validerProjet.addEventListener("click", function(){
		idProjet++;
		let nom = (document.getElementById('inputNom').value);
		let duree = (document.getElementById('inputDuree').value);
		let dateDeb = (document.getElementById('inputDateDeb').value);
		let dateFin = (document.getElementById('inputDateFin').value);
		monProjet = new Projet(idProjet, nom, duree, dateDeb, dateFin);
		lesProjets = new Projets();
		lesProjets.ajouterProjet(monProjet);
		formulaireProjets.creerRadio("radioProjets", monProjet.nomProjet, monProjet.nomProjet, monProjet.nomProjet, monProjet.nomProjet, "labelClass");
		fonctionFetchAjouterContrat(monProjet);

		let url = "../controleurs/ajouterProjet.php";
		let data = new Projet(null, document.getElementById('nomP'), document.getElementById('duree'), document.getElementById('dateDeb'), document.getElementById('dateFin'));

		fonctionFetchAjouterProjet(url, data);
	});

	let annulerProjet = document.getElementById('btnAnnuler');
	annulerProjet.addEventListener("click", function(){
		let parent = document.getElementById('divFormulaire');
		let enfant = document.getElementById('ajoutProjet');
		parent.removeChild(enfant);
	});
}
window.addEventListener("load", function(){
	ajouterBoutonNouveauProjet();
});