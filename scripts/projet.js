class Projet {
    constructor(id, nom, duree, dateDeb, dateFin) {
        this.idProjet = id;
        this.nomProjet = nom;
        this.dureeProjet = duree;
        this.dateDebProjet = dateDeb;
        this.dateFinProjet = dateFin;
        this.taches = [];
    }
}
Projet.prototype.ajouterTache = function (tache) {
    this.taches.push(tache);
}
Projet.prototype.chercherTache = function (idTache) {
    for (tache of this.taches) {
        if (idTache == tache.idTache) {
            return tache;
        }
    }
}