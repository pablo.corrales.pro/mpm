function fonctionFetchAjouterProjet(url, unProjet){
    let data = "unProjet=" + JSON.stringify(unProjet);
    fetch(url,
            {method: 'post',
                headers: {"Content-type": "application/x-www-form-urlenconded; charset: UTF-8"},
                body:data
    })
    then(response => {
        return response.text();
    }).then(text => {
        unProjet.idProjet = text;
        let unParent = document.getElementById("content");
        let divForm = document.getElementById("ajoutProjet");
        unParent.removeChild(divForm);
        afficherProjet(unProjet);

        return text;
    })
}