class Formulaire {
    constructor(id, nom, parentDOM, classe) {
        this.id = id;
        this.nom = nom;
        this.parentDOM = parentDOM;
        this.classe = classe;
    }
};
Formulaire.prototype.formulaireDOM;
Formulaire.prototype.creerLabel = function (id, texte, classe) {
    let elementHTML = document.createElement('label');
    elementHTML.className = classe;
    elementHTML.setAttribute("id", id);
    this.formulaireDOM.appendChild(elementHTML);
    let labelTexte = document.createTextNode(texte);
    elementHTML.appendChild(labelTexte);
}

Formulaire.prototype.creerInputTexte = function (id, name, classe, value) {
    let elementHTML = document.createElement('input');
    elementHTML.className = classe;
    elementHTML.setAttribute("id", id);
    elementHTML.setAttribute("name", name);
    elementHTML.setAttribute("type", "Text");
    elementHTML.setAttribute("value", value);
    this.formulaireDOM.appendChild(elementHTML);
}

Formulaire.prototype.creerBouton = function (id, name, classe, value) {
    let elementHTML = document.createElement('input');
    elementHTML.className = classe;
    elementHTML.setAttribute("id", id);
    elementHTML.setAttribute("name", name);
    elementHTML.setAttribute("value", value);
    elementHTML.setAttribute("type", "button");
    this.formulaireDOM.appendChild(elementHTML);
}

Formulaire.prototype.creerRadio = function (name, value, id, labelId, labelTexte, labelClass) {
    let elementHTML = document.createElement('input');
    elementHTML.setAttribute("type", "radio");
    elementHTML.setAttribute("name", name);
    elementHTML.setAttribute("value", value);
    elementHTML.setAttribute("id", id);
    this.formulaireDOM.appendChild(elementHTML);
    this.creerLabel(labelId, labelTexte, labelClass);
}

Formulaire.prototype.creerGroupeRadio = function (name, tabRadio) {
    for (btnRadio of tabRadio) {
        let elementHTML = document.createElement('input');
        elementHTML.setAttribute("type", "radio");
        elementHTML.setAttribute("name", name);
        elementHTML.setAttribute("value", btnRadio[0]);
        elementHTML.setAttribute("id", btnRadio[0]);
        this.formulaireDOM.appendChild(elementHTML);
        this.creerLabel(id, btnRadio[1], labelClass);
    }
}

Formulaire.prototype.retourLigne = function () {
    let elementHTML = document.createElement('p');
    this.formulaireDOM.appendChild(elementHTML);
}

Formulaire.prototype.ajouterDiv = function (id, classe) {
    let elementHTML = document.createElement('div');
    elementHTML.className = classe;
    elementHTML.setAttribute("id", id);
    this.formulaireDOM.appendChild(elementHTML);
}

Formulaire.prototype.creerFormulaire = function () {
    let divFormu = document.createElement('div');
    divFormu.setAttribute('id', this.id)
    divFormu.className = this.classe;
    this.parentDOM.appendChild(divFormu);
    let formulaire = document.createElement('form');
    formulaire.setAttribute('name', this.nom);
    divFormu.appendChild(formulaire);
    this.formulaireDOM = formulaire;
}

Formulaire.prototype.creerInputDate = function (id, name, classe) {
    let elementHTML = document.createElement('input');
    elementHTML.className = classe;
    elementHTML.setAttribute("id", id);
    elementHTML.setAttribute("name", name);
    elementHTML.setAttribute("type", "Date");
    this.formulaireDOM.appendChild(elementHTML);
}