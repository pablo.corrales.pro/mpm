class Tache {
    constructor(id, nom, duree, idProjet) {
        this.idTache = id;
        this.nomTache = nom;
        this.dureeTache = duree;
        this.idProjet = idProjet; this.tachesAnterieurs = []
    }
}
Tache.prototype.ajouterTacheAnterieure = function (uneTache) {
    this.tachesAnterieurs.push(uneTache);
}